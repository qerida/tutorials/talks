# talks

Frontend

* 2018-01-27 [Jake Archibald: In The Loop - JSConf.Asia 2018](https://www.youtube.com/watch?v=cCOL7MC4Pl0&t=508s) - 35:11 - The browsers's event Loop, tasks,microtasks,requestAnimationFrame,requestIdleCallback,events
* 2018-05-09 [PWA starter kit: build fast, scalable, modern apps with Web Components (Google I/O '18)](https://www.youtube.com/watch?v=we3lLo-UFtk&index=6&t=596s&list=PLbvFH9kn32iu3W3RD7QSKnNuQ5LKDgDTq) - 37:46
* 2018-06-26 [Where is CSS4? When is it coming out?](https://www.youtube.com/watch?v=Jtmkk6odggs) - 7:00 - About CSS specifications
* 2018-11-12 [Complex JS-heavy Web Apps, Avoiding the Slow (Chrome Dev Summit 2018)](https://www.youtube.com/watch?v=ipNW6lJHVEs) - sqoosh.app image compression PWA

Devops

* 2018-05-09 [Microservices in the Cloud with Kubernetes and Istio (Google I/O '18)](https://www.youtube.com/watch?v=gauOI0O9fRM&index=17&t=4s&list=PLbvFH9kn32iu3W3RD7QSKnNuQ5LKDgDTq) - 38:00
* 2017-09-21 [Kubernetes 101 Workshop: Deploying a Simple Web App onto Kubernetes](https://www.youtube.com/watch?v=H-FKBoWTVws&t=1216s) - 2:25:44 
* 


Other

* 2017-10-10 [Dan Abramov - The Melting Pot of JavaScript](https://www.youtube.com/watch?v=G39lKaONAlA)

# Blog posts

* 2018-08-12 [Future of Polymer](https://43081j.com/2018/08/future-of-polymer) - James Garbutt - Comparison of Lit and Polymer 2/3


# Timeless tutorials

2016-11-07 [var, let and const - What, why and how - ES6 JavaScript Features](https://www.youtube.com/watch?v=sjyJBL5fkp8&index=88&list=WL&t=0s) - Fun Fun Function